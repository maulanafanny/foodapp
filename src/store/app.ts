// Utilities
import { defineStore } from 'pinia'

export const useAppStore = defineStore('app', {
  state: () => ({
    login: {
      name: '',
      email: '',
    },
  }),
})
