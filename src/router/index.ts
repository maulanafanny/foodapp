// Composables
import { createRouter, createWebHistory } from 'vue-router'
import { useAppStore } from '@/store/app'

const routes = [
  {
    path: '/',
    name: 'Home',
    component: () => import('@/views/Home.vue'),
  },
  {
    path: '/login',
    name: 'Login',
    component: () => import('@/views/Login.vue'),
  },
  {
    path: '/about',
    name: 'About',
    component: () => import('@/views/VuetifyHome.vue'),
  },
  {
    path: '/logout',
    name: 'Logout',
    component: () => import('@/views/Login.vue'),
    beforeEnter: (to: any, _: any, next: (arg0: { name: string; }) => void) => {
      const store = useAppStore()
      store.login.name = ''
      store.login.email = ''
      next({ name: 'Login' })
    }
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
})

router.beforeEach((to, _, next) => {
  const store = useAppStore()
  if (to.name !== 'Login' && !store.login.name && !store.login.email) next({ name: 'Login' })
  else next()
})

export default router
